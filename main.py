def main():
    mass = [1, 5, 8, 3, 2, 4, 6, 9, 7]
    bubble(mass)
    print(str(mass))


def bubble(mass):
    mass_length = len(mass)  # определяем длину массива
    for i in range(mass_length):  # Внешний цикл
        for j in range(0, mass_length - i - 1):  # Внутренний цикл
            if mass[j] > mass[j + 1]:  # Меняем элементы местами
                temp = mass[j]
                mass[j] = mass[j + 1]
                mass[j + 1] = temp

    return mass


main()
